import os

import time
from pynamodb.attributes import BooleanAttribute, JSONAttribute, NumberAttribute, NumberSetAttribute, UnicodeAttribute, MapAttribute, \
    ListAttribute

from overtrack_models.orm.common import GlobalSecondaryIndexExt, OverTrackModel, make_gsi


class OverwatchShareSettings(MapAttribute):

    def get_value(self, value):
        if 'BOOL' in value:
            # migrate from when this field was a bool
            return super().serialize(OverwatchShareSettings(enabled=value['BOOL']))
        else:
            return super().get_value(value)

    __str__ = OverTrackModel.__str__

    enabled = BooleanAttribute(default=False)
    accounts = ListAttribute(null=True)
    include_quickplay = BooleanAttribute(default=True)


# noinspection PyAbstractClass
class User(OverTrackModel):

    class Meta:
        table_name = os.environ.get('USERS_TABLE', 'overtrack_users')
        region = 'us-west-2'

    key = UnicodeAttribute(attr_name='battletag', hash_key=True, null=False)

    user_id = NumberAttribute(attr_name='user-id', default=0)
    user_id_index: GlobalSecondaryIndexExt['User'] = make_gsi('user-id-index', user_id=user_id)

    twitch_id = UnicodeAttribute(null=True)
    twitch_id_index: GlobalSecondaryIndexExt['User'] = make_gsi(twitch_id=twitch_id)
    twitch_user = JSONAttribute(null=True)

    battlenet_id = NumberAttribute(null=True)
    battlenet_id_index: GlobalSecondaryIndexExt['User'] = make_gsi(battlenet_id=battlenet_id)
    battlenet_user = JSONAttribute(null=True)

    _username = UnicodeAttribute(attr_name='username', null=True)
    username_index: GlobalSecondaryIndexExt['User'] = make_gsi(username=_username)

    discord_id = UnicodeAttribute(null=True)
    discord_user = JSONAttribute(null=True)
    discord_id_index: GlobalSecondaryIndexExt['User'] = make_gsi(discord_id=discord_id)

    current_sr = NumberAttribute(attr_name='current-sr', null=True)
    twitch_account = UnicodeAttribute(attr_name='twitch-account', null=True)

    overwatch_games = NumberAttribute(attr_name='games-parsed', null=True)
    apex_games = NumberAttribute(null=True)
    apex_games_index: GlobalSecondaryIndexExt['User'] = make_gsi(apex_games=apex_games)
    valorant_games = NumberAttribute(null=True)

    daily_upload_limit = NumberAttribute(attr_name='daily-upload-limit', null=True)
    game_uploads_today = NumberAttribute(attr_name='game-uploads-today', null=True, default=0)
    telemetry_uploads_today = NumberAttribute(attr_name='telemetry-uploads-today', null=True, default=0)
    last_day_uploaded = NumberAttribute(attr_name='last-day-uploaded', null=True, default=0)

    current_upload_requested = NumberAttribute(attr_name='current-upload-requested', null=True)

    type = UnicodeAttribute(attr_name='type', null=True)
    superuser = BooleanAttribute(attr_name='superuser', null=True)

    account_created = NumberAttribute(attr_name='account-created', null=True)
    free = BooleanAttribute(attr_name='free', default=False, null=True)
    free_custom_games = BooleanAttribute(attr_name='free-custom-games', null=True, default=None)

    subscription_active = BooleanAttribute(attr_name='subscription-active', default=False)
    subscription_type = UnicodeAttribute(attr_name='subscription-type', null=True, default=None)

    stripe_customer_id = UnicodeAttribute(attr_name='stripe-customer-id', null=True, default=None)
    stripe_customer_email = UnicodeAttribute(attr_name='stripe-customer-username', null=True, default=None)
    stripe_subscription_id = UnicodeAttribute(attr_name='stripe-subscription-id', null=True, default=None)

    paypal_payer_email = UnicodeAttribute(attr_name='paypal-payer-email', null=True, default=None)
    paypal_payer_id = UnicodeAttribute(attr_name='paypal-payer-id', null=True, default=None)
    paypal_subscr_id = UnicodeAttribute(attr_name='paypal-subscr-id', null=True, default=None)
    paypal_subscr_date = UnicodeAttribute(attr_name='paypal-subscr-date', null=True, default=None)
    paypal_cancel_at_period_end = BooleanAttribute(attr_name='paypal-cancel-at-period-end', null=True, default=None)

    used_trial = BooleanAttribute(attr_name='used-trial', default=False, null=True)
    trial_active = BooleanAttribute(attr_name='trial-active', default=False, null=True)
    trial_games_remaining = NumberAttribute(attr_name='trial-games-remaining', null=True)
    trial_end_time = NumberAttribute(attr_name='trial-end-time', null=True)

    stream_key = UnicodeAttribute(attr_name='stream-key', null=True)
    twitch_overlay = UnicodeAttribute(attr_name='twitch-overlay', null=True)

    apex_one_time_webhook = JSONAttribute(null=True)
    apex_current_game = JSONAttribute(null=True)
    apex_games_public = BooleanAttribute(default=False)
    apex_seasons = NumberSetAttribute(default=set())
    apex_last_season = NumberAttribute(null=True)
    apex_last_game_ranked = BooleanAttribute(null=True)

    overwatch_seasons = NumberSetAttribute(default=set())
    overwatch_last_season = NumberAttribute(null=True)
    overwatch_games_public = OverwatchShareSettings(default=None, null=True)

    valorant_games_public = BooleanAttribute(null=True)

    client_last_started = NumberAttribute(null=True)

    __nonce = UnicodeAttribute(attr_name='nonce', null=True)
    __record_states = BooleanAttribute(attr_name='record-states', null=True)
    __demo = BooleanAttribute(attr_name='demo', null=True)

    @property
    def battletag(self) -> str:
        return self.key

    @property
    def username(self) -> str:
        if self._username:
            return self._username
        else:
            return self.battletag.replace('#', '-').replace('!', '')

    @username.setter
    def username(self, username: str) -> None:
        self._username = username

    @classmethod
    def get(cls, hash_key, **kwargs):
        """ :rtype: User """
        return super(User, cls).get(hash_key, **kwargs)

    def increment_game_uploads_today(self):
        return self.update({
            'game_uploads_today': {
                'action': 'add',
                'value': 1
            }
        })

    def increment_telemetry_uploads_today(self):
        return self.update({
            'telemetry_uploads_today': {
                'action': 'add',
                'value': 1
            }
        })

    @property
    def account_valid(self):
        return self.free or self.subscription_active or self.trial_valid

    @property
    def trial_valid(self):
        if not self.trial_active:
            return False
        else:
            if self.trial_games_remaining > 0 or self.trial_end_time > time.time():
                return True
            else:
                return False
