from typing import TypeVar, Generic, Dict, TYPE_CHECKING, Optional, Type
try:
    from typing import GenericMeta
except:
    class GenericMeta(type):
        pass

from pynamodb.pagination import ResultIterator

if TYPE_CHECKING:
    pass

from pynamodb.attributes import ListAttribute, MapAttribute, Attribute
from pynamodb.indexes import AllProjection, Projection, IndexMeta, Index
from pynamodb.models import Model


class TupleAttribute(ListAttribute):
    def __init__(self, length=None, hash_key=False, range_key=False, null=None, default=None, attr_name=None):
        super(TupleAttribute, self).__init__(hash_key=hash_key,
                                             range_key=range_key,
                                             null=null,
                                             default=default,
                                             attr_name=attr_name)
        self.length = length
        if self.default is not None and not isinstance(self.default, tuple):
            raise TypeError('Default value must be a tuple')

    def serialize(self, values):
        if self.length is not None and len(values) != self.length:
            raise TypeError('Expected tuple of length %d but had %d' % (self.length, len(values)))
        return super(TupleAttribute, self).serialize(values)

    def deserialize(self, value):
        if value is None:
            if self.null:
                return self.default
            else:
                raise TypeError('Got None but null=False')
        else:
            result = super(TupleAttribute, self).deserialize(value)
            if self.length is not None and len(result) != self.length:
                raise TypeError('Expected tuple of length %d but had %d' % (self.length, len(result)))
            return tuple(result)


# noinspection PyAbstractClass,PyUnresolvedReferences
class OverTrackModel(Model):
    def __str__(self) -> str:
        attributes = list(self._attributes.keys())
        # make `key` the first item
        key_names = [k for k, v in self._attributes.items() if v.is_hash_key]
        if len(key_names):
            key_name = key_names[0]
            attributes.remove(key_name)
            attributes.insert(0, key_name)
        items_str = ', '.join('%s=%s' % (attr, repr(getattr(self, attr))) for attr in attributes)
        return self.__class__.__name__ + '(' + items_str + ')'

    def asdict(self) -> Dict[str, object]:
        attributes = list(self._attributes.keys())
        # make `key` the first item
        key_names = [k for k, v in self._attributes.items() if v.is_hash_key]
        if len(key_names):
            key_name = key_names[0]
            attributes.remove(key_name)
            attributes.insert(0, key_name)
        attrs = {
            attr: getattr(self, attr) for attr in attributes
        }
        r = {}
        for k, v in attrs.items():
            if isinstance(v, MapAttribute):
                r[k] = OverTrackModel.asdict(v)
            elif isinstance(v, list):
                r[k] = [OverTrackModel.asdict(e) if isinstance(e, MapAttribute) else e for e in v]
            else:
                r[k] = v
        return r

    def refresh(self, consistent_read=False) -> None:
        # Fix for https://github.com/pynamodb/PynamoDB/issues/424
        # Only works on models with no range key

        hash_key_field = [k for k, v in self._attributes.items() if v.is_hash_key][0]
        assert len([k for k, v in self._attributes.items() if v.is_range_key]) == 0
        fresh = self.__class__.get(getattr(self, hash_key_field))

        for k in self._attributes:
            setattr(self, k, getattr(fresh, k))

    @classmethod
    def check_indexes(cls):
        for n in dir(cls):
            i = getattr(cls, n, None)
            if isinstance(i, Index):
                try:
                    next(i.scan(limit=1))
                except StopIteration:
                    pass


T = TypeVar('T')


class ResultIteratorExt(Generic[T], ResultIterator):

    def __next__(self) -> T:
        return ResultIterator.next(self)

    def __iter__(self) -> 'ResultIteratorExt[T]':
        return self


class GenericIndexMeta(GenericMeta, IndexMeta):
    def __init__(cls, name, bases, attrs, **kwargs):
        # Need to support **kwargs as GenericMeta wants to be able to init with tvars, args, origin, etc...
        # See typing.py:1136
        IndexMeta.__init__(cls, name, bases, attrs)


class GlobalSecondaryIndexExt(Generic[T], Index, metaclass=GenericIndexMeta):

    @classmethod
    def count(cls,
              hash_key,
              range_key_condition=None,
              filter_condition=None,
              consistent_read=False,
              **filters) -> int:
        """
        Count on an index
        """
        return cls.Meta.model.count(
            hash_key,
            range_key_condition=range_key_condition,
            filter_condition=filter_condition,
            index_name=cls.Meta.index_name,
            consistent_read=consistent_read,
            **filters
        )

    @classmethod
    def get(self,
            hash_key,
            range_key_condition=None,
            filter_condition=None,
            scan_index_forward=None,
            consistent_read=False,
            limit=None,
            page_size=None,
            last_evaluated_key=None,
            attributes_to_get=None,
            **filters) -> T:
        try:
            return next(self.Meta.model.query(
                hash_key,
                range_key_condition=range_key_condition,
                filter_condition=filter_condition,
                index_name=self.Meta.index_name,
                scan_index_forward=scan_index_forward,
                consistent_read=consistent_read,
                limit=limit,
                page_size=page_size,
                last_evaluated_key=last_evaluated_key,
                attributes_to_get=attributes_to_get,
                **filters
            ))
        except StopIteration:
            pass
        raise self.Meta.model.DoesNotExist(
            f'{self.Meta.model.__qualname__} matching search criteria does not exist '
            f'in index "{self.Meta.index_name}" '
        )

    @classmethod
    def query(self,
              hash_key,
              range_key_condition=None,
              filter_condition=None,
              scan_index_forward=None,
              newest_first=None,
              consistent_read=False,
              limit=None,
              last_evaluated_key=None,
              attributes_to_get=None,
              **filters) -> ResultIteratorExt[T]:
        """
        Queries an index
        """
        if scan_index_forward is None and newest_first is not None:
            scan_index_forward = not newest_first

        return self.Meta.model.query(
            hash_key,
            range_key_condition=range_key_condition,
            filter_condition=filter_condition,
            index_name=self.Meta.index_name,
            scan_index_forward=scan_index_forward,
            consistent_read=consistent_read,
            limit=limit,
            last_evaluated_key=last_evaluated_key,
            attributes_to_get=attributes_to_get,
            **filters
        )

    @classmethod
    def scan(self,
             filter_condition=None,
             segment=None,
             total_segments=None,
             limit=None,
             last_evaluated_key=None,
             page_size=None,
             consistent_read=None,
             **filters) -> ResultIteratorExt[T]:
        """
        Scans an index
        """
        return self.Meta.model.scan(
            filter_condition=filter_condition,
            segment=segment,
            total_segments=total_segments,
            limit=limit,
            last_evaluated_key=last_evaluated_key,
            page_size=page_size,
            consistent_read=consistent_read,
            index_name=self.Meta.index_name,
            **filters
        )


def make_gsi(
        index_name: Optional[str] = None,
        projection: Type[Projection] = AllProjection,
        **kwargs: Attribute
) -> GlobalSecondaryIndexExt:

    items = list(kwargs.items())
    if not 0 < len(items) <= 2:
        raise ValueError(f'Cannot create an index with {len(items)} attributes')
    key_name, key_attribute = items[0]
    range_name, range_attribute = items[1] if len(items) == 2 else (None, None)

    if not index_name:
        index_name = key_name
        if range_attribute:
            index_name += '-' + range_name
        index_name += '-index'

    class_name = ''.join(n.title() for n in index_name.replace('_', '-').split('-'))
    class_fields = {
        'Meta': type(
            'Meta',
            (object,),
            {
                'index_name': index_name,
                'projection': projection(),
                'read_capacity_units': 1,
                'write_capacity_units': 1,
            }
        ),
        key_name: key_attribute.__class__(hash_key=True, attr_name=key_attribute.attr_name),
    }
    if range_attribute:
        class_fields[range_name] = range_attribute.__class__(range_key=True, attr_name=key_attribute.attr_name)

    return type(
        class_name,
        (GlobalSecondaryIndexExt, ),
        class_fields,
    )()

