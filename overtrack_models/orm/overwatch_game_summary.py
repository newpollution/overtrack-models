import datetime
import os

from pynamodb.attributes import BooleanAttribute, NumberAttribute, UnicodeAttribute, ListAttribute

from overtrack_models.orm.common import GlobalSecondaryIndexExt, OverTrackModel, TupleAttribute, make_gsi


# noinspection PyAbstractClass
class OverwatchGameSummary(OverTrackModel):

    class Meta:
        table_name = os.environ.get('GAMES_TABLE', 'overtrack_games')
        region = 'us-west-2'

    key = UnicodeAttribute(attr_name='key', hash_key=True, null=False)
    user_id = NumberAttribute(attr_name='user-id')
    time = NumberAttribute(attr_name='time')
    viewable = BooleanAttribute(attr_name='viewable', null=True, default=True)

    duration = NumberAttribute(attr_name='duration', null=True)
    season = NumberAttribute(null=True)

    map = UnicodeAttribute(attr_name='map', null=True)
    attacking = BooleanAttribute(null=True)
    rounds = NumberAttribute(null=True)

    start_sr = NumberAttribute(attr_name='start-sr', null=True)
    end_sr = NumberAttribute(attr_name='end-sr', null=True)
    rank = UnicodeAttribute(attr_name='rank', null=True)

    game_type = UnicodeAttribute(attr_name='game-type', null=False, default='competitive')
    custom_game = BooleanAttribute(attr_name='custom-game', null=True)
    role = UnicodeAttribute(null=True)

    player_name = UnicodeAttribute(attr_name='player-name')
    player_battletag = UnicodeAttribute(attr_name='player-battletag', null=True)
    heroes_played = TupleAttribute(attr_name='heroes-played', null=True, default=None)

    score = TupleAttribute(attr_name='score', length=2, null=True, default=None)
    result = UnicodeAttribute(attr_name='result', null=True)

    frames_uri = UnicodeAttribute(null=True)
    sr_frame_uri = UnicodeAttribute(null=True)
    game_version = UnicodeAttribute(null=True)
    source = UnicodeAttribute(attr_name='source', null=True)

    notification_ids = ListAttribute(null=True)
    edited = BooleanAttribute(attr_name='edited', null=True, default=False)
    log_id = TupleAttribute(attr_name='log-id', length=3, null=True, default=None)

    user_id_time_index: GlobalSecondaryIndexExt['OverwatchGameSummary'] = make_gsi('user-id-time-index', user_id=user_id, time=time)
    season_time_index: GlobalSecondaryIndexExt['OverwatchGameSummary'] = make_gsi('season-time-index', season=season, time=time)
    user_id_player_name_index: GlobalSecondaryIndexExt['OverwatchGameSummary'] = make_gsi('user-id-player-name-index', user_id=user_id, player_name=player_name)

    @property
    def datetime(self):
        return datetime.datetime.fromtimestamp(self.time)

    def to_dict(self):
        data = {
            'key': self.key,
            'user_id': self.user_id,
            'time': self.time
        }
        if self.map is None:
            data.update({
                'player_name': self.player_name.upper().replace('0', 'O') if self.player_name else 'UNKNOWN',
                'parsing_failed': True
            })
        else:
            for attr in [
                'player_name',
                'map',
                'duration',
                'heroes_played',
                'result',
                'score',
                'start_sr',
                'end_sr',
                'rank',
                'viewable',
                'role'
             ]:
                data[attr] = getattr(self, attr)
            data['custom_game'] = bool(self.custom_game)
            if self.custom_game:
                data['game_type'] = 'custom'
            else:
                data['game_type'] = self.game_type or 'competitive'

        return data
