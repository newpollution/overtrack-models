import os
from typing import Optional

from overtrack_models.dataclasses.valorant import ValorantGame
from overtrack_models.orm.common import OverTrackModel, TupleAttribute, GlobalSecondaryIndexExt, make_gsi
from pynamodb.attributes import UnicodeAttribute, NumberAttribute, ListAttribute, BooleanAttribute, MapAttribute


class Stats(MapAttribute):
    kills = NumberAttribute()
    deaths = NumberAttribute()
    assists = NumberAttribute(null=True)


class Rounds(MapAttribute):
    attacking_first = BooleanAttribute()
    round_results = ListAttribute()
    attack_wins = NumberAttribute()
    defence_wins = NumberAttribute()

    def __str__(self):
        return (
            f'{self.__class__.__qualname__}('
            f'attacking_first={str(self.attacking_first)[0]}, '
            f'round_results=[{" ".join(["L", "W"][r] if r is not None else "?" for r in self.round_results)}], '
            f'attack_wins={self.attack_wins}, '
            f'defence_wins={self.defence_wins}'
            f')'
        )
    __repr__ = __str__


class ValorantGameSummary(OverTrackModel):
    class Meta:
        table_name = os.environ.get('APEX_GAME_TABLE', 'overtrack_valorant_games')
        region = os.environ.get('DYNAMODB_REGION', 'us-west-2')

    key = UnicodeAttribute(hash_key=True)

    user_id = NumberAttribute()
    timestamp = NumberAttribute()
    season_mode_id = NumberAttribute()
    game_mode = UnicodeAttribute(default='unrated')
    player_name = UnicodeAttribute(null=True)

    spectated = BooleanAttribute()

    won = BooleanAttribute(null=True)
    scrim = BooleanAttribute(default=False)
    score = TupleAttribute(2, null=True)
    duration = NumberAttribute()
    agent = UnicodeAttribute(null=True)
    map = UnicodeAttribute()
    compositions = ListAttribute()
    stats = Stats(null=True)
    rounds = Rounds(null=True)

    source = UnicodeAttribute(null=True)
    frames_uri = UnicodeAttribute(null=True)
    game_version = UnicodeAttribute(null=True)

    version = UnicodeAttribute()

    url = UnicodeAttribute(null=True)

    user_id_timestamp_index: GlobalSecondaryIndexExt['ValorantGameSummary'] = make_gsi(user_id=user_id, timestamp=timestamp)

    @classmethod
    def create(
        cls,
        game: ValorantGame,
        user_id: int,
        url: Optional[str] = None,
        source: Optional[str] = None,
        frames_uri: Optional[str] = None,
    ) -> 'ValorantGameSummary':
        return cls(
            key=game.key,

            user_id=user_id,
            timestamp=game.timestamp,
            season_mode_id=game.season_mode_id,
            game_mode=game.game_mode,
            player_name=game.teams.firstperson.name if game.teams.firstperson else None,

            spectated=game.spectated,

            won=game.won,
            scrim=game.rounds.has_game_resets,
            score=game.score,
            duration=game.duration,
            agent=game.teams.firstperson.agent if game.teams.firstperson else None,
            map=game.map,
            compositions=[
                [p.agent for p in team]
                for team in game.teams
            ],
            stats=Stats(
                kills=len(game.teams.firstperson.kills),
                deaths=len(game.teams.firstperson.deaths),
                assists=game.teams.firstperson.stats.assists if game.teams.firstperson.stats else None,
            ) if game.teams.firstperson else None,
            rounds=Rounds(
                attacking_first=game.rounds.attacking_first,
                round_results=[
                    r.won for r in game.rounds
                ],
                attack_wins=game.rounds.attack_wins,
                defence_wins=game.rounds.defence_wins,
            ),

            source=source,
            frames_uri=frames_uri,
            game_version=game.game_version,

            version=game.version,

            url=url,
        )


def main():
    ValorantGameSummary.create_table(billing_mode='PAY_PER_REQUEST')


if __name__ == '__main__':
    main()
