from typing import Dict, List, NamedTuple, Optional, Tuple

from dataclasses import dataclass

from overtrack_models.dataclasses.util import Literal

GameResult = Literal['WIN', 'LOSS', 'DRAW', 'UNKNOWN']

MapType = Literal['Escort', 'Hybrid', 'Assault', 'Control', 'Training']
StageName = Literal['A', 'B', 'C']


@dataclass
class Season:
    name: str
    start: float
    end: float

    index: Optional[int] = None
    display: bool = True
    off_season: bool = False
    is_222: bool = False

    filterable_by_index: bool = False

    def __contains__(self, timestamp: float) -> bool:
        return self.start <= timestamp < self.end


class ControlStage(NamedTuple):
    letter: StageName
    name: str


@dataclass
class Map:
    name: str
    type: Optional[MapType]

    @property
    def code(self) -> str:
        return ''.join(c for c in self.name.replace(' ', '_') if c in string.ascii_letters + string.digits + '_')

    @property
    def is_known(self) -> bool:
        return self.type is not None


@dataclass
class ControlMap(Map):
    stages: List[ControlStage]

    def __str__(self) -> str:
        return f'{self.__class__.__name__}(name={self.name}, type={self.type}, stages=...)'

    @property
    def stage_dict(self) -> Dict[StageName, str]:
        return {
            s.letter: f'{s.name} ({s.letter})' for s in self.stages
        }


@dataclass
class Mode:
    name: str
    is_arcade: bool = False
    is_vs_ai: bool = False

    @property
    def code(self) -> str:
        return self.name.upper().replace(' ', '')


StatType = Literal['maximum', 'average', 'best', 'duration']
Role = Literal['tank', 'damage', 'support']


@dataclass
class Stat:
    name: str
    v1_name: Optional[str] = None
    stat_type: StatType = 'maximum'
    is_percent: bool = False

    def __post_init__(self):
        if not self.v1_name:
            self.v1_name = self.name


@dataclass
class Hero:
    name: str
    key: str
    role: Role
    ult: Optional[str]
    can_heal: bool

    stats: Tuple[List[Stat], List[Stat]]

    ability_1: Optional[str] = None
    ability_2: Optional[str] = None
