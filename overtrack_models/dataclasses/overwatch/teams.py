from typing import Dict, List, Optional, Tuple, TYPE_CHECKING

from dataclasses import dataclass, field

from overtrack_models.dataclasses.util import Literal, s2ts
from overtrack_models.dataclasses.overwatch.killfeed import EliminationAssist, Kill, KillfeedAssist

if TYPE_CHECKING:
    from overtrack_models.dataclasses.overwatch.teamfights import PlayerStats

PlayerRank = Literal[
    'bronze',
    'silver',
    'gold',
    'platinum',
    'diamond',
    'master',
    'grandmaster',
    'top500',
    'placement'
]


@dataclass
class UltHeldPeriod:
    gained: float
    lost: float
    time_to_build: float
    used: bool
    hero: str
    stage: int

    player: Optional['Player'] = field(metadata={
        'migration': lambda data: None,
        'added_in': '2.4.0',
    })

    @property
    def held(self):
        return self.lost - self.gained

    def __str__(self) -> str:
        return f'UltHeldPeriod(' \
               f'gained={s2ts(self.gained, zpad=False)}, ' \
               f'lost={s2ts(self.lost, zpad=False)}, ' \
               f'held={s2ts(self.held, zpad=False)}, ' \
               f'time_to_build={s2ts(self.time_to_build, zpad=False)}, ' \
               f'used={self.used}, ' \
               f'hero={self.hero}, ' \
               f'stage={self.stage}' \
               f')'

    __repr__ = __str__


@dataclass
class HeroSeenEvent:
    timestamp: float
    hero: str
    source: str

    def __lt__(self, other: 'HeroSeenEvent') -> bool:
        return self.timestamp < other.timestamp

    def __str__(self) -> str:
        return f'HeroSeenEvent(' \
               f'timestamp={s2ts(self.timestamp, zpad=False)}, ' \
               f'hero={self.hero}, ' \
               f'source={self.source}' \
               f')'

    __repr__ = __str__


@dataclass
class HeroPlayedSegment:
    hero: str
    start_end: Tuple[float, float]
    stage: int

    @property
    def timestamp(self) -> float:
        return self.start

    @property
    def start(self) -> float:
        return self.start_end[0]

    @property
    def end(self) -> float:
        return self.start_end[1]

    @property
    def duration(self) -> float:
        return self.end - self.start

    def __str__(self) -> str:
        return f'HeroPlayedSegment(' \
               f'{self.hero}, ' \
               f'{s2ts(self.start, zpad=False)} -> {s2ts(self.end, zpad=False)} (duration={s2ts(self.duration)}), ' \
               f'stage={self.stage}' \
               f')'

    __repr__ = __str__

    def __lt__(self, other):
        return self.timestamp < other.timestamp


@dataclass
class Player:
    name: str
    blue_team: bool
    index: int
    rank: Optional[PlayerRank]
    name_correct: bool = True
    hero_tracked: bool = False

    kills: List[Kill] = field(default_factory=list)
    deaths: List[Kill] = field(default_factory=list)

    resurrects: List[Kill] = field(default_factory=list)
    resurrected: List[Kill] = field(default_factory=list)

    destructions: List[Kill] = field(default_factory=list)
    destroyed: List[Kill] = field(default_factory=list)

    elimination_assists: List[EliminationAssist] = field(default_factory=list)
    killfeed_assists: List[KillfeedAssist] = field(default_factory=list)

    ults: List[UltHeldPeriod] = field(default_factory=list)

    hero_seen_events: List[HeroSeenEvent] = field(default_factory=list, repr=False)
    names_seen: List[str] = field(default_factory=list, repr=False)

    hero_played_segments: Optional[List[HeroPlayedSegment]] = None
    hero_durations: Optional[Dict[str, float]] = None

    stats: Optional['PlayerStats'] = field(default=None, metadata={'added_in': '2.4.0'})
    stats_by_hero: Optional[Dict[str, 'PlayerStats']] = field(default=None, metadata={'added_in': '2.4.0'})

    @property
    def heroes(self) -> List[str]:
        r: List[str] = []
        for t, h, _ in sorted(self.hero_played_segments):
            if h not in r:
                r.append(h)
        return r

    @property
    def killfeed_events(self) -> List[Kill]:
        return sorted(self.kills + self.deaths + self.resurrected + self.resurrects + self.destructions + self.destroyed)

    @property
    def team_position_name(self) -> str:
        return f'{["Red", "Blue"][self.blue_team]} {self.index + 1}'


@dataclass
class Teams:
    owner: Optional[Player]
    owner_name: Optional[str]
    owner_battletag: Optional[str] = field(metadata={'fallback': None, 'added_in': '2.3.0'})
    name_from_metadata: bool

    blue: List[Player]
    red: List[Player]

    average_sr: Optional[Tuple[Optional[int], Optional[int]]] = field(metadata={'fallback': None, 'added_in': '2.3.0'})

    @property
    def teams(self) -> Tuple[List[Player], List[Player]]:
        return self.blue, self.red

    @property
    def all_players(self) -> List[Player]:
        return self.blue + self.red
