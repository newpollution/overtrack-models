from typing import Dict, List, Optional

from dataclasses import dataclass, field

from overtrack_models.dataclasses.apex.combat import Combat
from overtrack_models.dataclasses.apex.route import Route
from overtrack_models.dataclasses.apex.squad import Squad
from overtrack_models.dataclasses.apex.weapons import Weapons


@dataclass
class Rank:
    rank: Optional[str]
    rank_tier: Optional[str]
    rp: Optional[int]
    rp_change: Optional[int]


@dataclass
class ApexGame:
    key: str
    timestamp: float

    duration: float
    season: int
    match_started: float

    kills: int
    placed: int

    squad: Squad
    combat: Combat
    route: Route
    weapons: Weapons
    rank: Optional[Rank] = field(metadata={'fallback': None})

    scrims: Optional[str] = None
    solo: bool = False

    match_id: Optional[str] = None
    match_ids: List[Optional[str]] = field(default_factory=list)

    player_name: Optional[str] = None
    champion: Optional[Dict] = None
    images: Optional[Dict[str, str]] = None

    frames_count: Optional[int] = None
    valid: bool = True
