from typing import List

from dataclasses import dataclass


@dataclass
class WeaponStats:
    weapon: str
    time_held: float = 0
    time_active: float = 0

    knockdowns: int = 0


@dataclass
class Weapons:
    weapon_stats: List[WeaponStats]
