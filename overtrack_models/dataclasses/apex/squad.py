from typing import Optional, Tuple

from dataclasses import dataclass


@dataclass
class PlayerStats:
    kills: Optional[int] = None
    damage_dealt: Optional[int] = None
    survival_time: Optional[int] = None
    players_revived: Optional[int] = None
    players_respawned: Optional[int] = None

    # API-only fields
    shots_fired: Optional[int] = None
    shots_hit: Optional[int] = None
    damage_taken: Optional[int] = None

    rp: Optional[int] = None
    rp_change: Optional[int] = None


@dataclass
class Player:
    """
    A Player from a Squad.
    Each game (usually) has 3 Players forming a `Squad`.

    The initial name is provided, then player data from other sources (i.e. in frames)
    is derived by assigning data based on finding the best matching player from each other
    source based on the edit distance/ratio of names from those sources.

    Attributes
    -----------
    name: str
        The name of the player. May be corrected from the supplied if a higher-accuracy source is found.
    champion: str
        The champion that the player is playing.
    stats: Optional[PlayerStats]
        The stats for this player.
    is_owner: bool
        Whether the player is the first-person player of the game
    name_from_config: bool
        Whether `name` is provided ahead of time from *config* (instead of OCR).
        If True, the name is assumed fully accurate.
    name_matches_api: bool
        Whether `name` matches (or was updated to match) data provided from the Apex API.
        If True, the name can likely be assumed to be fully accurate.
    """

    name: str
    champion: Optional[str]
    stats: Optional[PlayerStats]
    is_owner: bool = False
    name_from_config: bool = False
    name_matches_api: bool = False


@dataclass
class Squad:
    player: Player
    squadmates: Tuple[Optional[Player], Optional[Player]]

    squad_kills: Optional[int]

