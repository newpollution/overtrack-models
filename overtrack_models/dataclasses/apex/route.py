from typing import List, NamedTuple, Optional, Tuple

from dataclasses import dataclass, field

Coordinates = Tuple[int, int]


@dataclass
class Ring:
    round: int
    center: Coordinates
    radius: int
    start_time: float
    end_time: float


class Location(NamedTuple):
    timestamp: float
    coordinates: Coordinates


@dataclass
class Route:
    map: str = field(metadata={'fallback': 'worlds_edge.s4'})
    locations: List[Location]
    time_landed: float
    landed_location_index: int
    landed_location: Coordinates
    landed_name: str
    locations_visited: List[str]
    rings: List[Optional[Ring]]

