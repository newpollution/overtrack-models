from typing import List, Optional, Tuple

from dataclasses import dataclass

from overtrack_models.dataclasses.util import s2ts


@dataclass
class CombatEvent:
    timestamp: float
    type: str
    inferred: bool = False
    weapon: Optional[str] = None
    location: Optional[Tuple[int, int]] = None

    def __str__(self) -> str:
        r = f'CombatEvent(ts={s2ts(self.timestamp)}, type={self.type}'
        if self.inferred:
            r += f', inferred=True'
        if self.weapon:
            r += f', weapon={self.weapon}'
        if self.location:
            r += f', location={self.location}'
        return r + ')'

    __repr__ = __str__


@dataclass
class Combat:
    eliminations: List[CombatEvent]
    knockdowns: List[CombatEvent]
    elimination_assists: List[CombatEvent]
    knockdown_assists: List[CombatEvent]
