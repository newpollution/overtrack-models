from typing import Any, Union, List

from dataclasses import dataclass, field


@dataclass
class _DepthCount:
    value: int


@dataclass
class Pointer:
    to: Any
    key: Union[str, int]
    from_: Union[dict, list]
    trace: List[Union[str, int]] = field(default_factory=list)
